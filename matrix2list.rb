#!/usr/bin/env ruby

if ARGV.size != 1
	puts "fajl"
	exit
end

input = File.read(ARGV[0])
lines = input.lines.map(&:chomp)

lines.collect! do |line|
	line.split
end

lines[0].unshift('X')

for i in 1...lines.length
	for j in i+1...lines.length
		next if lines[i][j] == '0'
		puts "#{lines[i][0]} #{lines[0][j]} #{lines[i][j]}"
	end
end