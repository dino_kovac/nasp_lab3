package hr.fer.nasp.lab3;

import java.util.*;

/**
 * Created by dino on 15/01/14.
 */
public class PrimMST {

    private Graph graph;

    public PrimMST(Graph graph) {
        this.graph = graph;
    }

    public List<Edge> findMST() {

        List<Edge> tree = new ArrayList<>();
        Set<Edge> edgeSet = new HashSet<>();
        List<Edge> neighbourEdges = new ArrayList<>();
        Set<Vertex> visited = new HashSet<>();
        visited.add(graph.getVertices().iterator().next());

        while (visited.size() < graph.getVertices().size()) {
            edgeSet.clear();
            neighbourEdges.clear();

            // find all edges neighbouring the visited vertices
            for (Vertex vertex : visited) {
                edgeSet.addAll(vertex.getEdges());
            }

            // get rid of the edges already in the tree
            edgeSet.removeAll(tree);

            // remove edges which connect vertices which are already visited
            Iterator<Edge> edgeIterator = edgeSet.iterator();
            while (edgeIterator.hasNext()) {
                Edge edge = edgeIterator.next();
                if (visited.contains(edge.getFirst()) && visited.contains(edge.getSecond())) {
                    edgeIterator.remove();
                }
            }

            // sort by size, ascending
            neighbourEdges.addAll(edgeSet);
            Collections.sort(neighbourEdges);

            // add the edge with the smallest weight
            if (!neighbourEdges.isEmpty()) {
                Edge edge = neighbourEdges.get(0);
                tree.add(edge);

                // mark the vertices visited
                visited.add(edge.getFirst());
                visited.add(edge.getSecond());
            }
        }

        return tree;
    }


}
