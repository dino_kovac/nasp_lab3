package hr.fer.nasp.lab3;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by dino on 12/01/14.
 */
public class Graph {

    private Set<Vertex> vertices;
    private Set<Edge> edges;

    public Set<Vertex> getVertices() {
        return vertices;
    }

    public Set<Edge> getEdges() {
        return edges;
    }

    public Graph() {
        vertices = new LinkedHashSet<>();
        edges = new LinkedHashSet<>();
    }

    public void addVertex(Vertex v) {
        vertices.add(v);
        edges.addAll(v.getEdges());
    }

    public void addEdge(Edge e) {
        edges.add(e);
        vertices.add(e.getFirst());
        vertices.add(e.getSecond());
    }

    public Vertex getVertex(Vertex v) {
        for (Vertex vertex : vertices) {
            if (v.equals(vertex)) {
                return vertex;
            }
        }

        return null;
    }

    public Edge getEdge(Edge e) {
        for (Edge edge : edges) {
            if (e.equals(edge)) {
                return edge;
            }
        }

        return null;
    }

}
