package hr.fer.nasp.lab3;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Created by dino on 14/01/14.
 */
public class MSTHelper {

    private static List<Edge> cycle = new ArrayList<>();
    private static Collection<Edge> edgeCollection;
    private static List<Edge> visited = new ArrayList<>();

    public static List<Edge> getCycle(Collection<Edge> edges) {
        cycle.clear();
        visited.clear();

        edgeCollection = edges;

        Edge startEdge = edges.iterator().next();

        detectCycle(startEdge, null);

        return cycle;
    }

    private static boolean detectCycle(Edge currentEdge, Edge lastVisited) {

        // base case
        if (visited.contains(currentEdge)) {
            // congratulations, it's a cycle
            return true;
        }

        visited.add(currentEdge);

        Set<Edge> neighbourEdges;
        if (lastVisited == null) {
            neighbourEdges = new HashSet<>(currentEdge.getFirst().getEdges());
            neighbourEdges.addAll(currentEdge.getSecond().getEdges());
        } else {
            Vertex lastVertex = currentEdge.getOtherVertex(currentEdge.getCommonVertex(lastVisited));
            neighbourEdges = new HashSet<>(lastVertex.getEdges());
        }
        neighbourEdges.remove(currentEdge);
        neighbourEdges.retainAll(edgeCollection);

        for (Edge neighbour : neighbourEdges) {
            if (detectCycle(neighbour, currentEdge)) {
                cycle.add(currentEdge);
                return true;
            }
        }

        return false;
    }

    public static void exportAsDot(Collection<Edge> edges, Collection<Edge> mst, String filename) {

        try (PrintWriter writer = new PrintWriter(filename, "UTF-8")) {

            writer.println("graph mst {");

            for (Edge edge : edges) {

                writer.print("\t");

                writer.print(edge.getFirst().getName() + " -- " + edge.getSecond().getName());

                if (mst.contains(edge)) {
                    writer.print(" [label=\"" + edge.getWeight() + "\" fontcolor=red color=red]");
                } else {
                    writer.print(" [label=\"" + edge.getWeight() + "\" fontcolor=dimgray]");
                }
                writer.println(";");

            }

            writer.println("}");

        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

}
