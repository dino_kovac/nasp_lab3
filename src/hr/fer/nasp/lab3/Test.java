package hr.fer.nasp.lab3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Created by dino on 12/01/14.
 */
public class Test {

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("Need filename for argument! Aborting.");
            System.exit(-1);
        }

        String filename = args[0];

        Graph g = new Graph();
        String[] elem;
        int weight;

        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {

            String line = br.readLine();

            Vertex first;
            Vertex second;
            Edge edge;

            while (line != null) {
                elem = line.split(" ");
                assert elem.length == 3;
                weight = Integer.parseInt(elem[2]);

                first = new Vertex(elem[0]);
                second = new Vertex(elem[1]);

                // prevent duplicate instances of the same vertex
                if (g.getVertices().contains(first)) {
                    first = g.getVertex(first);
                }

                if (g.getVertices().contains(second)) {
                    second = g.getVertex(second);
                }

                edge = new Edge(weight, first, second);

                if (g.getEdges().contains(edge)) {
                    edge = g.getEdge(edge);
                }

                g.addEdge(edge);

                line = br.readLine();
            }

            DijkstraMST dijkstraMST = new DijkstraMST(g);
            List<Edge> mst = dijkstraMST.findMST();
            MSTHelper.exportAsDot(g.getEdges(), mst, "dijkstra_mst.dot");

            KruskalMST kruskalMST = new KruskalMST(g);
            mst = kruskalMST.findMST();
            MSTHelper.exportAsDot(g.getEdges(), mst, "kruskal_mst.dot");

            PrimMST primMST = new PrimMST(g);
            mst = primMST.findMST();
            MSTHelper.exportAsDot(g.getEdges(), mst, "prim_mst.dot");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
