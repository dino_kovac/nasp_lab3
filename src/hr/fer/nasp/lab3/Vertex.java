package hr.fer.nasp.lab3;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by dino on 12/01/14.
 */
public class Vertex implements Comparable<Vertex> {

    private String name;
    private Set<Vertex> neighbours;
    private Set<Edge> edges;

    private Vertex() {
        // not allowed
    }

    public Vertex(String name) {
        this.name = name;
        neighbours = new LinkedHashSet<>();
        edges = new LinkedHashSet<>();
    }

    public void addEdge(Edge e) {
        edges.add(e);

        if (this.equals(e.getFirst())) {
            neighbours.add(e.getSecond());
        } else if (this.equals(e.getSecond())) {
            neighbours.add(e.getFirst());
        }
    }

    public void addNeighbour(Vertex v, int weight) {
        neighbours.add(v);

        edges.add(new Edge(weight, this, v));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Vertex> getNeighbours() {
        return neighbours;
    }

    public Set<Edge> getEdges() {
        return edges;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || !(obj instanceof Vertex)) {
            return false;
        }

        Vertex other = (Vertex) obj;

        if (name.equals(other.name)) {
            return true;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public int compareTo(Vertex o) {
        return name.compareTo(o.name);
    }
}
