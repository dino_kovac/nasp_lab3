package hr.fer.nasp.lab3;

/**
 * Created by dino on 12/01/14.
 */
public class Edge implements Comparable<Edge> {

    private int weight;
    private Vertex first;
    private Vertex second;

    public Edge() {
        // not allowed
    }

    public Edge(int weight, Vertex first, Vertex second) {
        this.weight = weight;
        this.first = first;
        this.second = second;
        this.first.addEdge(this);
        this.second.addEdge(this);
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Vertex getFirst() {
        return first;
    }

    public void setFirst(Vertex first) {
        this.first = first;
        first.addEdge(this);
    }

    public Vertex getSecond() {
        return second;
    }

    public void setSecond(Vertex second) {
        this.second = second;
        second.addEdge(this);
    }

    public boolean contains(Vertex v) {

        if (v.equals(first) || v.equals(second)) {
            return true;
        }

        return false;
    }

    public Vertex getCommonVertex(Edge e) {

        if (e == null) {
            return null;
        }

        if (first.equals(e.first) || first.equals(e.second)) {
            return first;
        }

        if (second.equals(e.first) || second.equals(e.second)) {
            return second;
        }

        return null;
    }

    public Vertex getOtherVertex(Vertex v) {

        if (first.equals(v)) {
            return second;
        }

        if (second.equals(v)) {
            return first;
        }

        return null;
    }

    @Override
    public String toString() {
        return first.toString() + "--[" + weight + "]--" + second.toString();
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null || !(obj instanceof Edge)) {
            return false;
        }

        Edge other = (Edge) obj;

        if (weight != other.weight) {
            return false;
        }

        if (!first.equals(other.first) && !first.equals(other.second)) {
            return false;
        }

        if (!second.equals(other.first) && !second.equals(other.second)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {

        int hashCode = weight;

        if (first != null) {
            hashCode += first.hashCode();
        }

        if (second != null) {
            hashCode += second.hashCode();
        }

        return hashCode;
    }

    @Override
    public int compareTo(Edge o) {
        return Integer.compare(weight, o.weight);
    }
}
