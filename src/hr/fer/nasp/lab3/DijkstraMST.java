package hr.fer.nasp.lab3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dino on 15/01/14.
 */
public class DijkstraMST {

    private Graph graph;

    public DijkstraMST(Graph graph) {
        this.graph = graph;
    }

    public List<Edge> findMST() {

        List<Edge> tree = new ArrayList<>();

        for (Edge edge : graph.getEdges()) {
            tree.add(edge);

            List<Edge> cycle = MSTHelper.getCycle(tree);

            if (!cycle.isEmpty()) {
                Collections.sort(cycle, Collections.reverseOrder());
                tree.remove(cycle.get(0)); // remove maximum weight edge
            }
        }

        return tree;
    }

}
