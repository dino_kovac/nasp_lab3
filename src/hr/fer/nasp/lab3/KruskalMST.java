package hr.fer.nasp.lab3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dino on 15/01/14.
 */
public class KruskalMST {

    private Graph graph;

    public KruskalMST(Graph graph) {
        this.graph = graph;
    }

    public List<Edge> findMST() {

        List<Edge> tree = new ArrayList<>();
        List<Edge> edges = new ArrayList<>(graph.getEdges());

        Collections.sort(edges);

        for (Edge edge : edges) {
            tree.add(edge);

            if (!MSTHelper.getCycle(tree).isEmpty()) {
                tree.remove(edge);
            }
        }

        return tree;
    }


}
